import React, {useState} from 'react'
import {
   View,
   Text,
   TouchableOpacity,
   TextInput,
   StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'

const AddData = ({
   navigation,
   route
}) => {

   const [name, setName] = useState('');
   const [email, setEmail] = useState('');
   const [address, setAddress] = useState('');
   const [phone, setPhone] = useState('');
   const storeData = () => {
    var date = new Date();
    var dataProduct = [...product]
    const data = {
        id: date.getMilliseconds(),
        fullName: name,
        email: email,
        address: address,
        phone: phone
    }
    dataProduct.push(data)
    dispatch({type: 'ADD_DATA', data: dataProduct})
    navigation.goBack()
}



   return(
       <View style={{flex: 1, backgroundColor: '#fff'}}>
           <View style={styles.header}>
               <TouchableOpacity
                   style={{
                       width: '15%',
                       justifyContent: 'center',
                       alignItems: 'center'
                   }}
                   onPress={() => navigation.goBack()}
               >
                   <Icon
                       name="arrowleft"
                       size={25}
                       color="#fff"
                   />
               </TouchableOpacity>
               <View style={{
                   width: '70%',
                   justifyContent: 'center',
                   alignItems: 'center'
               }}>
                   <Text style={{fontSize: 16, fontWeight: '600', color: '#fff'}}>
                       Tambah Data
                   </Text>
               </View>
               <View style={{width: '15%'}} />
           </View>
           <View style={{
               flex: 1,
               padding: 15
           }}>
               <TextInput
placeholder='Masukkan Nama Lengkap'
style={styles.txtInput}
value={name}
onChangeText={(text) => setName(text)}
/>
<TextInput
placeholder='Masukkan Email'
style={styles.txtInput}
value={email}
onChangeText={(text) => setEmail(text)}
keyboardType='email-address'
/>
<TextInput
placeholder='Masukkan Nomor Telepone'
style={styles.txtInput}
value={phone}
onChangeText={(text) => setPhone(text)}
keyboardType='number-pad'
/>
<TextInput
placeholder='Masukkan Alamat'
style={[
    styles.txtInput, {
        height: 120,
        textAlignVertical: 'top'
    }
]}
multiline                   
value={address}
onChangeText={(text) => setAddress(text)}
/>
<TouchableOpacity
style={styles.btnAdd}
>
<Text style={{fontSize: 14, color: '#fff', fontWeight: '600'}}>
    Tambah
</Text>
</TouchableOpacity>
</View>
</View>
)
}

const styles = StyleSheet.create({
btnAdd: {
marginTop: 20,
width: '100%',
backgroundColor: '#43a047',
borderRadius: 6,
       justifyContent: 'center',
       alignItems: 'center',
       paddingVertical: 15
   },
   txtInput: {
       width: '100%',
       borderRadius: 6,
       borderColor: '#dedede',
       borderWidth: 1,
       paddingHorizontal: 10,
       marginTop: 20
   },
   header: {
       width: '100%',
       flexDirection: 'row',
       alignItems: 'center',
       backgroundColor: '#3f51b5',
       paddingVertical: 15
   }
})

export default AddData;
