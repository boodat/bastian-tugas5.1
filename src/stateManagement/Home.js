import React from "react";
import { View, Text } from "react-native";
import userData from './utils/reducers/authReducer'

const Home =() =>{
    // console.log('cek userData', userData);
    return (
        <View style={{
            backgroundColor: 'white',
            padding: 30,
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
        }}>
            <Text>
                HOME
            </Text>
            
        </View>
    )
}

export default Home;